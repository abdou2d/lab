var Episodes = React.createClass({

	getInitialState () {
      return {
      search: ''
      }
    },
 
  updateSearch(event) {
    this.setState ({search: event.target.value });
  },



  render() {
  	let filteredEpisodes = this.props.episodes.filter(
        (episode) => {
          return episode.title.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1 || 
          		 episode.content.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
       }
     );

    return ( 
      	<div>
      		<div className="search">
	      		<div className="medium-4 large-3 columns float-right">
			        <input type="text"
			          value={this.state.search}
			          onChange={this.updateSearch}
			          placeholder= 'Search...'
			          />
		          </div>
	          </div>

	        {filteredEpisodes.map(function(episode){
	         return (
	            <div key={episode.id} className="large-12 columns episode">

	            	<div className="screenshot medium-3 large-3 columns small-12 text-center medium-text-right float-right">
	              		<a href={"episodes/" + episode.id}><img src={episode.img} /></a>
					</div>

		            <div className="description medium-9 large-9 columns small-12 text-center medium-text-right">
		            	<h2><a href={"episodes/" + episode.id}>{episode.title}</a></h2>
		            	<h6><small> {moment(episode.created_at).format('MMM D, YYYY')}</small> - {episode.position}</h6>
		              	<p>{episode.content.substr(0, 180)}</p>			
					</div>
	            </div>
	         )
      		})} 
    	</div>
    )
  }
});
