class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

	attr_accessor :login

	belongs_to :role
	has_one :profile, dependent: :destroy

	before_validation :set_default_role
	before_create :build_profile

	def admin?
	self.role.name == "admin"
	end

	def registered?
	self.role.name == "registered"
	end
	
	def name
  		"#{first_name} #{last_name}"
  	end

  	validate :validate_username

  	def validate_username
		if User.where(email: username).exists?
		  errors.add(:username, :invalid)
		end
	end

	validates :email, 
	        presence: true,
	        uniqueness: { :case_sensitive => false },
	        format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\Z/ }

	validates :username, 
	        presence: true,  
	        length: { in: 3..30 },
	        uniqueness: { :case_sensitive => false },
	        format: { with: /\A[a-zA-Z0-9]+\Z/ }

	def self.find_for_database_authentication(warden_conditions)
		conditions = warden_conditions.dup
		if login = conditions.delete(:login)
		  where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
		elsif conditions.has_key?(:username) || conditions.has_key?(:email)
		  where(conditions.to_h).first
		end
	end
	
	def update_with_password(params={}) 
	  if params[:password].blank? 
	    params.delete(:password) 
	    params.delete(:password_confirmation) if params[:password_confirmation].blank? 
	  end 
	  update_attributes(params) 
	end

	private
	
	def set_default_role
	  self.role ||= Role.find_by_name('registered')
	end

end
