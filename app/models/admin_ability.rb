class AdminAbility
  include CanCan::Ability
  def initialize(user)
    if user && user.registered?
      can :access, :rails_admin
      can :manage, :all   
    end
  end
end