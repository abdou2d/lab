class EpisodesController < ApplicationController
  def index
  	if params[:tag]
      @episodes = Episode.tagged_with(params[:tag])
    else
      @episodes = Episode.all
    end
  end

  def show
  	@episode = Episode.find(params[:id])
  end
end
