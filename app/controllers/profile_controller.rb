class ProfileController < ApplicationController
	
  def show
  	@profile =  User.where(username: params[:id]).first!.profile
  end
end
