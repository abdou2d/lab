Rails.application.routes.draw do

  devise_for :users
  mount RailsAdmin::Engine => '/abdou2d', as: 'rails_admin'
  root 'front#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :profile
  resources :episodes
end
