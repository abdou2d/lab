class CreateTaggings < ActiveRecord::Migration[5.1]
  def change
    create_table :taggings do |t|
      t.integer :tag_id
      t.integer :episode_id

      t.timestamps
    end

    add_index :taggings, [:episode_id, :tag_id]
  end
end
