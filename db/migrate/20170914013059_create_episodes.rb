class CreateEpisodes < ActiveRecord::Migration[5.1]
  def change
    create_table :episodes do |t|
      t.boolean :paid,            default: false
      t.string :title
      t.string :slug
      t.text :content
      t.text :notes
      t.string :position
      t.string :video
      t.string :pro_video
      t.string :img
      t.string :fhd_download
      t.string :files
      t.datetime :published_at

      t.timestamps
    end
    add_index :episodes, :slug, unique: true
  end
end
