class RenameRoleToName < ActiveRecord::Migration[5.1]
  def change
  	rename_column :roles, :role, :name
  end
end
